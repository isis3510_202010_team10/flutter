import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mercado_pulgas/blocs/detail_store/detail_store.dart';
import 'package:mercado_pulgas/blocs/detail_seller/detail_seller.dart';
import 'package:mercado_pulgas/blocs/products_store/products_store.dart';
import 'package:mercado_pulgas/blocs/category/categories.dart';
import 'package:mercado_pulgas/models/product.dart';
import 'package:mercado_pulgas/resources/product/firebase_product_repository.dart';
import 'package:mercado_pulgas/resources/seller/firebase_seller_repository.dart';
import 'package:mercado_pulgas/resources/store/firebase_store_repository.dart';
import 'package:mercado_pulgas/resources/category/firebase_category_repository.dart';
import 'package:mercado_pulgas/ui/screens/screens.dart';

class Routes {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => HomeScreen());
      case '/detail-product':
        if (args is Product) {
          return MaterialPageRoute(builder: (_) {
            return ProductDetailScreen(product: args);
          });
        }
        return _errorScreen();

      case '/detail-store':
        if (args is List<String>) {
          return MaterialPageRoute(builder: (_) {
              return MultiBlocProvider(
                  providers: [
                    BlocProvider<DetailStoreBloc>(
                      create: (context) {
                        return DetailStoreBloc(
                          storeRepository: FirebaseStoreRepository(),
                        )..add(LoadStoreById(args[0]));
                      },
                    ),
                    BlocProvider<DetailSellerBloc>(
                      create: (context) {
                        return DetailSellerBloc(
                          sellerRepository: FirebaseSellerRepository(),
                        )..add(LoadSellerByName(args[1]));
                      },
                    ),
                    BlocProvider<CategoriesBloc>(
                      create: (context) {
                        return CategoriesBloc(
                          categoryRepository: FirebaseCategoryRepository(),
                        )..add(LoadCategoriesByStore([args[2]]));
                      },
                    ),
                    BlocProvider<ProductsStoreBloc>(
                      create: (context) {
                        return ProductsStoreBloc(
                          productRepository: FirebaseProductRepository(),
                        )..add(LoadProductsByStore(int.parse(args[3])));
                      },
                    )
                  ],
                  child: StoreScreen()
              );
            }

            );
        }
        return _errorScreen();

      default:
        return _errorScreen();
    }
  }

  static Route<dynamic> _errorScreen() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
          appBar: AppBar(
            title: Text("Error 404"),
          ),
          body: Center(
            child: Text('Error de Navegación'),
          )
      );
    });
  }
}
