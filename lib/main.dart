import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:mercado_pulgas/blocs/simple_bloc_delegate.dart';
import 'package:mercado_pulgas/blocs/store/stores.dart';
import 'package:mercado_pulgas/blocs/connectivity/connectivity.dart';
import 'package:mercado_pulgas/resources/product/firebase_product_repository.dart';

import 'package:mercado_pulgas/resources/store/firebase_store_repository.dart';
import 'package:mercado_pulgas/routes.dart';
import 'package:mercado_pulgas/blocs/product/products.dart';

void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return
      MultiBlocProvider(
          providers: [
            BlocProvider<StoresBloc>(
              create: (context) {
                return StoresBloc(
                  storeRepository: FirebaseStoreRepository(),
                )..add(LoadStores());
              },
            ),
            BlocProvider<ProductsBloc>(
              create: (context) {
                return ProductsBloc(
                  productRepository: FirebaseProductRepository(),
                )..add(LoadProducts());
              },
            ),
            BlocProvider<ConnectivityBloc>(
              create: (context) {
                return ConnectivityBloc()..add(LoadConnectivityStatus());
              },
            )
          ],
          child: MaterialApp(
            title: 'Mercado de las Pulgas',
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
                fontFamily: 'Muli',
                primarySwatch: Colors.blue,
                primaryColor: Color(0xFF214a04),
                accentColor: Color(0xFFf18f01),
                buttonColor: Color(0xFF3fa535),
                textTheme: TextTheme(
                  headline: TextStyle(
                    fontFamily: 'Muli',
                    fontSize: 34.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.black
                  ),
                  title: TextStyle(
                      fontFamily: 'Muli',
                      fontSize: 26.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black
                  ),
                  subtitle: TextStyle(
                      fontFamily: 'Muli',
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black
                  ),
                  body1: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'Muli',
                      fontWeight: FontWeight.normal
                  ),
                  body2: TextStyle(
                    fontSize: 18.0,
                    fontFamily: 'Muli',
                    fontWeight: FontWeight.w600
                  ),
                  button: TextStyle(
                    color: Colors.white,
                    fontSize: 14.0,
                    fontFamily: 'Muli',
                    fontWeight: FontWeight.w600,
                  ),
                ),
            ),
          initialRoute: '/',
          onGenerateRoute: Routes.generateRoute,
          )
      );
  }
}

