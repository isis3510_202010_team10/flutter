import 'dart:async';

import 'package:mercado_pulgas/models/store.dart';

abstract class StoresRepository {

  Stream<List<Store>> stores();


}