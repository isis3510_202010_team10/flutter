import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mercado_pulgas/models/store.dart';
import 'package:mercado_pulgas/models/store_entity.dart';
import 'package:mercado_pulgas/resources/store/stores_repository.dart';

class FirebaseStoreRepository implements StoresRepository{
  final storeCollection = Firestore
      .instance
      .collection('Stores');

  @override
  Stream<List<Store>> stores () {
    return storeCollection
        .snapshots()
        .map((snapshot) {
      return snapshot.documents
          .map((doc) => Store.fromEntity(StoreEntity.fromSnapshot(doc)))
          .toList();
    });
  }

  Stream<Store> storeById (String id) {
    return storeCollection
        .document(id)
        .snapshots()
        .map((doc) => Store.fromEntity(StoreEntity.fromSnapshot(doc)));

  }
}