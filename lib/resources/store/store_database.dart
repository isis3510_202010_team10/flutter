import 'package:mercado_pulgas/models/store.dart';
import 'package:mercado_pulgas/models/store_entity.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class StoreDatabase {
  static Database database;

  initDB () async {
    database = await openDatabase(
      join(await getDatabasesPath(), 'store_database.db'),
      onCreate: (db, version) {
        return db.execute(
          """CREATE TABLE 
          stores(id TEXT PRIMARY KEY,
          name TEXT,
          description TEXT, 
          imgUrl TEXT,
          location TEXT,
          storeNumber INTEGER,
          categories TEXT,
          favorite INTEGER)""",
        );
      },
      version: 8,
    );
  }

  Future<void> insertStore(StoreEntity store) async {
    await database.insert(
      'stores',
      store.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> insertFavoriteStore(StoreEntity store) async {
    final resultado = await database.insert(
      'stores',
      store.toMapDatabase(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<StoreEntity>> stores() async {
    final List<Map<String, dynamic>> maps = await database.query('stores');

    return List.generate(maps.length, (i) {
      return StoreEntity(
          id: maps[i]['id'],
          name: maps[i]['name'],
          description: maps[i]['description'],
          imgUrl: maps[i]['imgUrl'],
          location: maps[i]['location'],
          storeNumber: maps[i]['storeNumber'],
          favorite: maps[i]['favorite']
      );
    });
  }

  Future<List<StoreEntity>> storeById(StoreEntity store) async {

    final List<Map<String, dynamic>> maps = await database
        .query(
        'stores',
        where: '"id" = ?',
        whereArgs: [store.id]);

    return List.generate(maps.length, (i) {
      return StoreEntity(
          id: maps[i]['id'],
          name: maps[i]['name'],
          description: maps[i]['description'],
          imgUrl: maps[i]['imgUrl'],
          location: maps[i]['location'],
          storeNumber: maps[i]['storeNumber'],
        favorite: maps[i]['favorite']
      );
    });
  }

  Future<List<StoreEntity>> favoriteStores() async {

    final List<Map<String, dynamic>> maps = await database
        .query('stores', where: '"favorite" = 1');
    print('Buscando favoritos');
    return List.generate(maps.length, (i) {

      final entity =  StoreEntity(
        id: maps[i]['id'],
        name: maps[i]['name'],
        description: maps[i]['description'],
        imgUrl: maps[i]['imgUrl'],
        location: maps[i]['location'],
        storeNumber: maps[i]['storeNumber'],
        favorite: maps[i]['favorite']
      );
      print(entity);
      return entity;
    });
  }

  Future<void> updateFavoriteStore(StoreEntity store) async {
    print('Update Favorite Store');
    print(store);
    await database.update(
      'stores',
      store.toMap(),
      where: "id = ?",
      whereArgs: [store.id],
    );
  }

}

