import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mercado_pulgas/models/product.dart';
import 'package:mercado_pulgas/models/product_entity.dart';

class FirebaseProductRepository {
  final productCollection = Firestore
      .instance
      .collection('Products');

  Stream<List<Product>> products () {
    return productCollection
        .limit(10)
        .snapshots()
        .map((snapshot) {
      return snapshot.documents
          .map((doc) => Product.fromEntity(ProductEntity.fromSnapshot(doc)))
          .toList();
    });
  }

  Stream<List<Product>> productsByStore(int storeNumber) {
    return productCollection
        .where('Store_number', isEqualTo: storeNumber)
        .snapshots()
        .map((snapshot) {
      return snapshot.documents
          .map((doc) => Product.fromEntity(ProductEntity.fromSnapshot(doc)))
          .toList();
    });
  }
}