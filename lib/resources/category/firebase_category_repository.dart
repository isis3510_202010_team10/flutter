import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mercado_pulgas/models/category.dart';
import 'package:mercado_pulgas/models/category_entity.dart';

class FirebaseCategoryRepository {
  final categoryCollection = Firestore
      .instance
      .collection('Categoria');

  @override
  Stream<List<Category>> categoriesByStore(List<String> categories) {
    return categoryCollection
        .where('Nombre', isEqualTo: categories[0])
        .snapshots()
        .map((snapshot) {
      return snapshot.documents
          .map((doc) => Category.fromEntity(CategoryEntity.fromSnapshot(doc)))
          .toList();
    });
  }
}