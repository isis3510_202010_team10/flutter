import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mercado_pulgas/models/seller.dart';
import 'package:mercado_pulgas/models/seller_entity.dart';

class FirebaseSellerRepository {
  final sellerCollection = Firestore
      .instance
      .collection('Vendedores');

  @override
  Stream<List<Seller>> sellers() {
    return sellerCollection
        .limit(10)
        .snapshots()
        .map((snapshot) {
      return snapshot.documents
          .map((doc) => Seller.fromEntity(SellerEntity.fromSnapshot(doc)))
          .toList();
    });
  }

  Stream<List<Seller>> sellerByName(String name) {
    return sellerCollection
        .where('Nombre', isEqualTo: name)
        .snapshots()
        .map((snapshot) {
      return snapshot.documents
          .map((doc) => Seller.fromEntity(SellerEntity.fromSnapshot(doc)))
          .toList();
    });
  }
}