import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProductDetailScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final initial = CupertinoPageScaffold(
      child:  Container(
        width: double.infinity,
        height: 250.0,
        child: Text('Crear una meta'),
      ),

      navigationBar: CupertinoNavigationBar(
        leading: Icon(CupertinoIcons.back),
          middle: Text('Crear una meta'),
      ),

    );

    return CupertinoTabScaffold(
      tabBuilder: (context, i) => initial,
      tabBar: CupertinoTabBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.phone),
            title: Text('Tel')
          ),
          BottomNavigationBarItem(
              icon: Icon(CupertinoIcons.book),
              title: Text('mass')
          )
        ],
      ),
    );

  }
}