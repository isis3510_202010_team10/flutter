import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:io' show Platform;

import 'package:mercado_pulgas/widgets/home.dart';
import 'package:mercado_pulgas/widgets/store_list.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>{
  int _currentIndex = 1;

  final tabs = [
    StoreList(),
    Home(),
    StoreList()
  ];

  Widget createBottomBar() {
    final tapManager = (index) {
        setState(() {
          _currentIndex = index;
        });
    };

    final bottomBarAndroid = BottomNavigationBar(
      currentIndex: _currentIndex,
      onTap: tapManager,
      items: [
        BottomNavigationBarItem(
            icon: Icon(Icons.book),
            title: Text('Mapa')
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Inicio')
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            title: Text('Tiendas')
        )
      ],
    );

    final bottomBarIOS = CupertinoTabBar(
      currentIndex: _currentIndex,
      onTap: tapManager,
      items: [
        BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.book),
            title: Text('Mapa')
        ),
        BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.home),
            title: Text('Inicio')
        ),
        BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.shopping_cart),
            title: Text('Tiendas')
        )
      ],
    );

    return Platform.isIOS
        ? bottomBarIOS
        : bottomBarAndroid;
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
        ),
        body: tabs[_currentIndex],
        bottomNavigationBar: createBottomBar(),
      );
  }
}