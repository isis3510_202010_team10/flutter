import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StoreScreen extends StatelessWidget {

  Widget _getStoreItem(BuildContext context) {
    final storeTitle = Container(
      margin: EdgeInsets.symmetric(vertical: 10.0),
      child: Text(
        'Puesto 164',
        style: TextStyle(
          fontSize: 35.0,
          fontWeight: FontWeight.bold,
          color: Colors.black
        ),
      )
    );

    final sellerTitle = Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
      height: 40.0,
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(width: 1.0, color: Color(0xFF000000)),
          bottom: BorderSide(width: 1.0, color: Color(0xFF000000)),
        )
      ),
      child: Text(
        'Información del vendedor',
        style: TextStyle(
          fontSize: 20.0,
          color: Theme.of(context).primaryColor
        ),
      ),
    );

    final sellerDescription = Row(
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(10.0),
          height: 100.0,
          width: 100.0,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage('https://images.pexels.com/photos/3645369/pexels-photo-3645369.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260')
              )
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Astrid Suarez',
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.w600
              )
            ),
            Text(
              'a.suarez\@gmail\.com',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.normal
                )
            ),
            Text(
                '\(+54\) 3125046253',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.normal
                )
            ),
          ],
        )
      ],
    );


    return Container(
      width: double.infinity,
      child: Column(        
        children: <Widget>[
          storeTitle,
          sellerTitle,
          sellerDescription
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tienda'),
      ),
      body: _getStoreItem(context),
    );
  }
}