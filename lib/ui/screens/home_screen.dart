import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:mercado_pulgas/ui/widgets/DataSearch.dart';
import 'dart:io' show Platform;

import 'package:mercado_pulgas/ui/widgets/home.dart';
import 'package:mercado_pulgas/ui/widgets/store_list.dart';
import 'package:mercado_pulgas/ui/widgets/map.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>{
  int _currentIndex = 1;

  final tabs = [
    Map(),
    Home(),
    StoreList()
  ];

  Widget _createBottomBar() {
    final tapManager = (index) {
      setState(() {
        _currentIndex = index;
      });
    };

    final bottomBarAndroid = BottomNavigationBar(
      currentIndex: _currentIndex,
      onTap: tapManager,
      items: [
        BottomNavigationBarItem(
            icon: Icon(Icons.book),
            title: Text('Mapa')
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Inicio')
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            title: Text('Tiendas')
        )
      ],
    );

    final bottomBarIOS = CupertinoTabBar(
      currentIndex: _currentIndex,
      onTap: tapManager,
      items: [
        BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.book),
            title: Text('Mapa')
        ),
        BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.home),
            title: Text('Inicio')
        ),
        BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.shopping_cart),
            title: Text('Tiendas')
        )
      ],
    );

    return Platform.isIOS
        ? bottomBarIOS
        : bottomBarAndroid;
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        title: Text("FlohMarkt"),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.search),onPressed: (){
            showSearch(context: context, delegate: DataSearch());
          },)
        ],
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: tabs[_currentIndex],
      bottomNavigationBar: _createBottomBar(),
    );
  }
}
