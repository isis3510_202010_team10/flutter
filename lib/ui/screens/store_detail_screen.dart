import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mercado_pulgas/blocs/detail_store/detail_store.dart';
import 'package:mercado_pulgas/models/store.dart';
import 'package:mercado_pulgas/ui/widgets/categories_carousel.dart';
import 'package:mercado_pulgas/ui/widgets/loading_progress.dart';
import 'package:mercado_pulgas/ui/widgets/products_by_store_grid.dart';
import 'package:mercado_pulgas/ui/widgets/seller_description.dart';

class StoreScreen extends StatelessWidget {

  Widget _getSubTitle(BuildContext context, String subtitle){
    return Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
      height: 40.0,
      decoration: BoxDecoration(
          border: Border(
            top: BorderSide(width: 1.0, color: Color(0xFF000000)),
            bottom: BorderSide(width: 1.0, color: Color(0xFF000000)),
          )
      ),
      child: Text(
        subtitle,
        style: TextStyle(
            fontSize: 20.0,
            color: Theme.of(context).primaryColor
        ),
      ),
    );
  }

  Widget _getStoreItem(BuildContext context, Store store) {
    final storeTitle = Container(
      margin: EdgeInsets.symmetric(vertical: 10.0),
      child: Text(
        'Puesto ${store.storeNumber}',
        style: Theme.of(context).textTheme.headline
      )
    );

    return ListView(
      children: <Widget>[
        Container(
          width: double.infinity,
          child: Column(
            children: <Widget>[
              storeTitle,
              _getSubTitle(context, 'Información del vendedor'),
              SellerDescription(),
              _getSubTitle(context, 'Categorias de la tienda'),
              CategoriesCarousel(),
              _getSubTitle(context, 'Productos de la tienda'),
            ],
          ),
        ),
        ProductsByStoreGrid()


      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DetailStoreBloc, DetailStoresState>(
        builder: (context, state) {
          if (state is StoreLoadSuccess) {
            final store = state.store;
            return Scaffold(
              appBar: AppBar(
                title: Text('Tienda'),
              ),
              body: _getStoreItem(context, store),
            );
          }else {
            return LoadingProgress();
          }
        }
    );
  }
}