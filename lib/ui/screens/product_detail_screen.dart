import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:mercado_pulgas/models/product.dart';

class ProductDetailScreen extends StatelessWidget {
  final Product product;

  ProductDetailScreen({this.product});

  Widget _getProductItem(BuildContext context) {
    return ListView(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 10.0),
                child:Text(
                  product.name,
                  style: Theme.of(context).textTheme.headline,
                ),
              ),
              SizedBox(height: 15.0),
              Hero(
                tag: 'Un nombre tag',
                child: CachedNetworkImage(
                  fit: BoxFit.fill,
                  height: 200,
                  width: 150,
                  progressIndicatorBuilder: (context, url, progress) =>
                    CircularProgressIndicator(value: progress.progress),
                  imageUrl: product.imgUrl,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 15.0),
                child: Text(
                  '\$${product.price}',
                  style: TextStyle(
                    fontFamily: 'Muli',
                    fontSize: 28.0,
                    color: Theme.of(context).accentColor
                  ),
                ),
              ),
              /*Container(
                margin: EdgeInsets.only(top: 15.0),
                child: Text(
                    product.name,
                  style: Theme.of(context).textTheme.title,
                ),
              ),*/
              Container(
                margin: EdgeInsets.only(top: 15.0),
                width: MediaQuery.of(context).size.width - 50.0,
                child: Text(
                    product.description,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.body1
                ),
              )
            ],
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Producto'),
      ),
      body: _getProductItem(context) ,
    );
  }
}