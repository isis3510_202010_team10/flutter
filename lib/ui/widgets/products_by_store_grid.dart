import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mercado_pulgas/blocs/products_store/products_store.dart';
import 'package:mercado_pulgas/ui/widgets/loading_progress.dart';
import 'package:mercado_pulgas/ui/widgets/product_item.dart';

class ProductsByStoreGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProductsStoreBloc, ProductsStoreState>(
        builder: (context, state) {
          if (state is ProducsStoreLoadSuccess) {
            final wihtProduct = (MediaQuery.of(context).size.width - 40)/2;
            final aspectRatio = (wihtProduct / (wihtProduct * (1.0 + 1/3)) );
            final products = state.products;
            return  GridView.count(
                  scrollDirection: Axis.vertical,
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  childAspectRatio: aspectRatio,
                  crossAxisCount: 2,
                  children: List.generate(products.length, (index) {
                    return Container(
                        child: ProductItem(
                          product: products[index],
                          width: wihtProduct,
                        )
                    );
                  })
            );
          }else {
            return LoadingProgress();
          }
        }
    );
  }
}