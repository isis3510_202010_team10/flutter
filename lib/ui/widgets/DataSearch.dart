import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mercado_pulgas/ui/widgets/map.dart';
import 'package:mercado_pulgas/models/globals.dart' as global;

class DataSearch extends SearchDelegate<String>{

  Map mapaR= Map();

  String busqueda = "";


  final results=[
    "Joyería y Bisutería",
    "Productos orgánicos",
    "Gemas",
    "Art. domésticos",
    "Escultura",
    "Numismática y filatelia",
    "Artesanías y oficios",
    "Antigüedades"
  ];
  final recentResults=[
    "Joyería y Bisutería",
    "Productos orgánicos",
    "Gemas",
    "Art. domésticos",
    "Escultura",
    "Numismática y filatelia",
    "Artesanías y oficios",
    "Antigüedades"
  ];
  @override
  List<Widget> buildActions(BuildContext context) {

    return [
      IconButton(icon: Icon(Icons.clear), onPressed: (){
        query = "";
      })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {

    return IconButton(
      icon: AnimatedIcon(
        icon:AnimatedIcons.menu_arrow,
          progress:transitionAnimation,
      ),
    onPressed: (){
        close(context, null);
    });
  }



  @override
  Widget buildResults(BuildContext context) {

    List<int> agregar =[];
    if (busqueda == null && query == null) {

    }
    else if (double.tryParse(query) != null)
      {

        double numberr=double.tryParse(query);
        Firestore
            .instance
            .collection('Stores')
            .where("Store_number",isEqualTo:numberr)
            .snapshots()
            .listen((data)=>
            data.documents.forEach((doc) => agregar.add((doc["Store_number"]))));
      }
    else
      {
        Firestore
            .instance
            .collection('Stores')
            .where("Categories",isGreaterThanOrEqualTo:busqueda)
            .snapshots()
            .listen((data)=>
            data.documents.forEach((doc) => agregar.add((doc["Store_number"]))));
      }

    global.setDatos(agregar);
    return Map();



  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestionList = query.isEmpty?recentResults:results.where((p)=>p.contains(query)).toList();
    return ListView.builder(itemBuilder: (context,index)=>ListTile(
      onTap: (){
        busqueda= suggestionList[index];
        showResults(context);
      },
      leading: Icon(Icons.add),
      title: Text(suggestionList[index]),
    ),itemCount: suggestionList.length,
    );
  }

}