import 'package:flutter/material.dart';

class NoConnection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Image(
            image: AssetImage('assets/images/wifioff.png'),
          ),
          Text(
              'No hay conexión a internet',
            style: Theme.of(context).textTheme.subtitle,
          )
        ],
      )
    );
  }
}