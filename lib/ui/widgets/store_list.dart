import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mercado_pulgas/blocs/store/stores.dart';
import 'package:mercado_pulgas/models/store.dart';
import 'package:mercado_pulgas/models/store_entity.dart';
import 'package:mercado_pulgas/ui/widgets/connectivity.dart';
import 'package:mercado_pulgas/ui/widgets/loading_progress.dart';
import 'store_item.dart';

class StoreList extends StatelessWidget {

  Widget roundStore(Store store) {
    return  Container(
      width: 90.0,
      margin: EdgeInsets.all(10.0),
      child: Column(
        children: <Widget>[
          CircleAvatar(
            radius: 35.0,
            backgroundImage: CachedNetworkImageProvider(store.imgUrl),
          ),
          SizedBox(height: 6.0),
          Text(
            store.name,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                fontSize: 18.0
            ),
          ),
        ],
      ),
    );
  }

  getFavorites(List<Store> favorites) {
    print('Longitud de favoritos  ${favorites.length}');
    final isEmptyFavorites = favorites.length == 0;
    final noFavoritesMessage = Container(
      height: 20.0,
      child: Center(
        child: Text(
            'Aun no tienes favoritos'
        ),
      ),
    );
    return Container(
      height: isEmptyFavorites ? 20.0 : 120.0,
      child: isEmptyFavorites
        ? noFavoritesMessage
        : ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: favorites.length,
              itemBuilder: (BuildContext context, int i) {
                return roundStore(favorites[i]);
              }
          )
    );
  }

  Widget _subtitle(BuildContext context, String subtitle) {
    return Row(
      children: <Widget>[
        Text(
          subtitle,
          style: Theme.of(context).textTheme.title
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return  BlocBuilder<StoresBloc, StoresState>(
      builder: (context, state) {
        if (state is StoresLoadSuccess) {
          final storesLoad = state.stores;
          final favoritesLoad = state.favorites;
          return Container(
              padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 0.0),
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  Text(
                    'Tiendas',
                    style: Theme.of(context).textTheme.headline,
                  ),
                  _subtitle(context, 'Favoritos'),
                  getFavorites(favoritesLoad),
                  _subtitle(context, 'Lista tiendas'),
                  Expanded(
                    child: Connectivity(
                        onConnectivity: Container(
                        width: MediaQuery.of(context).size.width-30.0,
                        child: ListView.builder(
                          itemCount: storesLoad.length,
                          itemBuilder: (BuildContext context, int index) {
                            return StoreItem(
                                store: storesLoad[index].toEntity(),
                            );
                          },
                        ),
                      ),
                    )
                  ),
                ],
              )
          );
        } else {
          return LoadingProgress();
        }
      },
    );
  }
}
