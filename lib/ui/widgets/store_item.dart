import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mercado_pulgas/models/store.dart';

import 'package:mercado_pulgas/resources/store/store_database.dart';
import 'package:mercado_pulgas/models/store_entity.dart';
import 'package:mercado_pulgas/blocs/store/stores.dart';

class StoreItem extends StatefulWidget {
  final StoreEntity store;

  const StoreItem({ Key key, this.store }) : super(key:key);

  @override
  _StoreItemState createState() => _StoreItemState();
}

class _StoreItemState extends State<StoreItem> {
  StoreDatabase db = StoreDatabase();
  Icon favIcon = Icon(Icons.favorite_border);

  Future isFavorite(StoreEntity store) async {
    await db.initDB();
    var result = await db.storeById(store);
    if (result.length >0) {
      return result[0].isFavorite();
    }
    return false;

  }

   _onFavoritePress(BuildContext context, StoreEntity store) {
    return () {
      if (store.isFavorite() ){
        print('Era favorito---------');
        store.removeFavorite();
      } else {
        print('Era no favorito---------');
        store.addFavorite();
      }

      BlocProvider.of<StoresBloc>(context).add(
        StoreUpdateFavorite(Store.fromEntity(store)),
      );
    };
  }

  @override
  Widget build(BuildContext context) {
    isFavorite(widget.store)
      .then((icon){
        setState(() {
          if (icon) {
            widget.store.addFavorite();
            favIcon = Icon(Icons.favorite);
          } else {
            widget.store.removeFavorite();
            favIcon = Icon(Icons.favorite_border);
          }
        });
      });

    final description = 'Ubicacion: ${widget.store.location}'
      +'\n${widget.store.description}'
      +'\nPuesto de ventas 🔥';

    final photo = Container(
      margin: EdgeInsets.only(
          top: 10.0,
          left: 20.0,
          bottom: 10.0
      ),
      width: 80.0,
      height: 80.0,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
          image: DecorationImage(
              fit: BoxFit.cover,
              image: CachedNetworkImageProvider(
                  widget.store.imgUrl
              )
          )
      ),
    );

   return  GestureDetector(
      onTap: () => Navigator.of(context)
          .pushNamed('/detail-store',
          arguments: [
            widget.store.id,
            widget.store.name,
            widget.store.categories[0],
            '${widget.store.storeNumber}'
          ]
      ),
      child: Card(
          child: Container(
              margin: EdgeInsets.symmetric(vertical: 10.0),
              child:  Row(
                children: <Widget>[
                  photo,
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(
                            left: 15.0
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width - 201.0,
                              child: Text(
                                widget.store.name,
                                textAlign: TextAlign.left,
                                overflow: TextOverflow.ellipsis,
                                style: Theme.of(context).textTheme.body2,
                              ),
                            ),
                            IconButton(
                              icon: favIcon,
                              onPressed: _onFavoritePress(context, widget.store),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width - 170.0,
                        margin: EdgeInsets.only(
                            left: 15.0,
                            right: 10.0
                        ),
                        child: Text(
                          description,
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context).textTheme.body1,
                        ),
                      )
                    ],
                  )
                ],
              )
          )
      ),
    );
  }
}