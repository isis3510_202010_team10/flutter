import 'package:flutter/material.dart';
import 'package:mercado_pulgas/models/product.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ProductItem extends StatelessWidget {
  final Product product;
  final double width;

  ProductItem({this.product, this.width = 150.0 });

  @override
  Widget build(BuildContext context) {

    final productImage = Container(
      height: width*(2/3),
      decoration: BoxDecoration(

        shape: BoxShape.rectangle,
        image: DecorationImage(
          fit: BoxFit.cover,
          image: CachedNetworkImageProvider(product.imgUrl)
        )
      ),
      child: Row(),
    );

    final productName = Container(
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      margin: EdgeInsets.symmetric(vertical: 5.0),
      child: Text(
        product.name,
        style: Theme.of(context).textTheme.subtitle,
        textAlign: TextAlign.center,
      ),
    );

    final productPrice = Container(
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      margin: EdgeInsets.symmetric(vertical: 0.0),
      child: Text(
        '\$${product.price}',
        style: TextStyle(
            color: Theme.of(context).accentColor,
            fontSize: 16.0,
            fontWeight: FontWeight.bold
        ),
        textAlign: TextAlign.center,
      ),
    );

    final productButton = FlatButton(
      padding: EdgeInsets.all(4.0),
      onPressed: () => Navigator.of(context)
          .pushNamed('/detail-product',
          arguments: product
      ),
      color: Theme.of(context).buttonColor,
      textColor: Colors.white,
      child: Text(
        'VER',
        style: Theme.of(context).textTheme.button,
      ),
    );
    return Container(
      margin: EdgeInsets.all(10.0),
      width: this.width,
      height: this.width * (1.0 + 2/3),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            offset: Offset(0.0, 2.0),
            blurRadius: 6.0,
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          productImage,
          productName,
          productPrice,
          productButton
        ],
      ),
    );
  }
}
