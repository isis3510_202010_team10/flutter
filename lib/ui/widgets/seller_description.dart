import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:mercado_pulgas/blocs/detail_seller/detail_seller.dart';
import 'package:mercado_pulgas/models/seller.dart';
import 'package:mercado_pulgas/ui/widgets/loading_progress.dart';

class SellerDescription extends StatelessWidget {

  Widget _sellerDescription(BuildContext context, Seller seller) {
    final profileImg = 'assets/images/profile.png';
    return Row(
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(10.0),
          height: 100.0,
          width: 100.0,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage(profileImg)
              )
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
                seller.name,
                style: Theme.of(context).textTheme.body2
            ),
            Text(
              'a.suarez\@gmail\.com\n'
                  +'\(+54\) 3125046253',
              style: Theme.of(context).textTheme.body1,
            ),
          ],
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DetailSellerBloc, DetailSellerState>(
        builder: (context, state) {
          if (state is SellerLoadSuccess) {
            final sellers = state.seller;
            if (sellers.length>0) {
              return _sellerDescription(context, sellers[0]);
            } else {
              return Text(
                  'No hay información acerca de este vendedor',
                  style: Theme.of(context).textTheme.body2,
              );
            }
          } else {
            return LoadingProgress();
          }
        }
    );
  }
}