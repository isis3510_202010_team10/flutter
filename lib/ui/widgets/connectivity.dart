import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mercado_pulgas/blocs/connectivity/connectivity.dart';
import 'package:mercado_pulgas/ui/widgets/loading_progress.dart';
import 'package:mercado_pulgas/ui/widgets/no_connection.dart';

class Connectivity extends StatelessWidget {
  final Widget onConnectivity;

  Connectivity({this.onConnectivity});

  @override
  Widget build(BuildContext context) {

    return BlocBuilder<ConnectivityBloc, ConnectivityState>(
      builder: (context, state) {
        if (state is ConnectivityLoading){
          return LoadingProgress();
        } else if (state is ConnectivityOn) {
          return onConnectivity;
        } else if (state is ConnectivityFailure) {
          return NoConnection();
        } else {
          return Center(
            child: Text('Ah ocurrido un error al comprobar la conexión'),
          );
        }
      },
    );
  }
}