import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mercado_pulgas/blocs/product/products.dart';
import 'package:mercado_pulgas/ui/widgets/connectivity.dart';
import 'package:mercado_pulgas/ui/widgets/loading_progress.dart';

import 'banner_img.dart';
import 'product_carousel.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProductsBloc, ProductsState> (
      builder: (context, state) {
        if (state is ProductsLoadSuccess) {
          final productsLoad = state.products;
          final int middleList = productsLoad.length%2 == 0
              ? (productsLoad.length/2).floor()
              : ((productsLoad.length + 1)/2).floor();

          return ListView(
            children: <Widget>[
              BannerImg(),
              Connectivity(
                onConnectivity: ProductCarousel(
                  nameListProducts:'Recomendaciones',
                  products: productsLoad.sublist(0,middleList),
                ),
              ),
              Connectivity(
                onConnectivity: ProductCarousel(
                  nameListProducts:'Populares',
                  products: productsLoad.sublist(middleList + 1),
                ),
              ),
            ],
          );
        } else {
          return LoadingProgress();
        }
      },
    );
  }
}
