import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mercado_pulgas/models/product.dart';

import 'package:mercado_pulgas/ui/widgets/product_item.dart';

class ProductCarousel extends StatelessWidget {
  final String nameListProducts;
  final List<Product> products;

  ProductCarousel({this.nameListProducts, this.products});

  @override
  Widget build(BuildContext context) {
    final nameListProductsText = Text(
      nameListProducts,
      style: Theme.of(context).textTheme.title,
    );

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: 15.0,
              vertical: 10.0
          ),
          child: nameListProductsText,
        ),
        Container(
          height: 250.0,
          child: ListView.builder(
            padding: EdgeInsets.symmetric(horizontal: 15.0),
            scrollDirection: Axis.horizontal,
            itemCount: products.length,
            itemBuilder: (BuildContext context, int index) {
              return ProductItem(product: products[index]);
            },
          ),
        )
      ],
    );
  }
}
