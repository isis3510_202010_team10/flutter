import 'package:flutter/material.dart';
import 'package:mercado_pulgas/models/globals.dart' as global;

class Map extends StatefulWidget {

  @override
  _MapState createState() => new _MapState();

}

class _MapState extends State<Map> {

  List<StoreMap> listaStores= new List(13);

  consultaBase(String consulta){
    print(consulta);

  }

  @override
  Widget build(BuildContext context) {


    setState(() {
      for (int i=0; i<listaStores.length;i++)
      {
        listaStores[i]=(new StoreMap());
      }
      if(global.lista.length>0)
        {
      listaStores[0].setNumber(46);
      if (global.lista[global.ultimo].contains(listaStores[0].storeNumber)){
        listaStores[0].setColor(Color(0xFF214a04));
      }
      else
        {
          listaStores[0].setColor(Color(0xFF3fa535));
        }
      listaStores[1].setNumber(47);
      if (global.lista[global.ultimo].contains(listaStores[1].storeNumber)){
        listaStores[1].setColor(Color(0xFF214a04));
      }
      else
      {
        listaStores[1].setColor(Color(0xFF3fa535));
      }
      listaStores[2].setNumber(48);
      if (global.lista[global.ultimo].contains(listaStores[2].storeNumber)){
        listaStores[2].setColor(Color(0xFF214a04));
      }
      else
      {
        listaStores[2].setColor(Color(0xFF3fa535));
      }
      listaStores[3].setNumber(50);
      if (global.lista[global.ultimo].contains(listaStores[3].storeNumber)){
        listaStores[3].setColor(Color(0xFF214a04));
      }
      else
      {
        listaStores[3].setColor(Color(0xFF3fa535));
      }
      listaStores[4].setNumber(51);
      if (global.lista[global.ultimo].contains(listaStores[4].storeNumber)){
        listaStores[4].setColor(Color(0xFF214a04));
      }
      else
      {
        listaStores[4].setColor(Color(0xFF3fa535));
      }
      listaStores[5].setNumber(52);
      if (global.lista[global.ultimo].contains(listaStores[5].storeNumber)){
        listaStores[5].setColor(Color(0xFF214a04));
      }
      else
      {
        listaStores[5].setColor(Color(0xFF3fa535));
      }
      listaStores[6].setNumber(160);
      if (global.lista[global.ultimo].contains(listaStores[6].storeNumber)){
        listaStores[6].setColor(Color(0xFF214a04));
      }
      else
      {
        listaStores[6].setColor(Color(0xFF3fa535));
      }
      listaStores[7].setNumber(161);
      if (global.lista[global.ultimo].contains(listaStores[7].storeNumber)){
        listaStores[7].setColor(Color(0xFF214a04));
      }
      else
      {
        listaStores[7].setColor(Color(0xFF3fa535));
      }
      listaStores[8].setNumber(162);
      if (global.lista[global.ultimo].contains(listaStores[8].storeNumber)){
        listaStores[8].setColor(Color(0xFF214a04));
      }
      else
      {
        listaStores[8].setColor(Color(0xFF3fa535));
      }
      listaStores[9].setNumber(163);
      if (global.lista[global.ultimo].contains(listaStores[9].storeNumber)){
        listaStores[9].setColor(Color(0xFF214a04));
      }
      else
      {
        listaStores[9].setColor(Color(0xFF3fa535));
      }
      listaStores[10].setNumber(194);
      if (global.lista[global.ultimo].contains(listaStores[10].storeNumber)){
        listaStores[10].setColor(Color(0xFF214a04));
      }
      else
      {
        listaStores[10].setColor(Color(0xFF3fa535));
      }
      listaStores[11].setNumber(195);
      if (global.lista[global.ultimo].contains(listaStores[11].storeNumber)){
        listaStores[11].setColor(Color(0xFF214a04));
      }
      else
      {
        listaStores[11].setColor(Color(0xFF3fa535));
      }
      listaStores[12].setNumber(197);
      if (global.lista[global.ultimo].contains(listaStores[12].storeNumber)){
        listaStores[12].setColor(Color(0xFF214a04));
      }
      else
      {
        listaStores[12].setColor(Color(0xFF3fa535));
      }
    }});
    return Scaffold(
        body: Stack(
          children:<Widget>[
            Positioned(
              top:5,
              left:10,
              child: ButtonTheme(
                padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 10),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: Theme.of(context).accentColor,
                  onPressed: () {},
                  child: Text(
                    "46A",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
        ),
            Positioned(
              top:5,
              left:105,
              child: ButtonTheme(
                padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 10),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: listaStores[0].getColor(),
                  onPressed: () {},
                  child: Text(
                    "46",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top:5,
              left:205,
              child: ButtonTheme(
                padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 10),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: Theme.of(context).accentColor,
                  onPressed: () {},
                  child: Text(
                    "45",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top:5,
              left:305,
              child: ButtonTheme(
                padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 10),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: Theme.of(context).accentColor,
                  onPressed: () {},
                  child: Text(
                    "44",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top:55,
              left:10,
              child: ButtonTheme(
                padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 10),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: listaStores[1].getColor(),
                  onPressed: () {},
                  child: Text(
                    "47",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top:105,
              left:10,
              child: ButtonTheme(
                padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 10),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: listaStores[2].getColor(),
                  onPressed: () {},
                  child: Text(
                    "48",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top:155,
              left:10,
              child: ButtonTheme(
                padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 10),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: Theme.of(context).accentColor,
                  onPressed: () {},
                  child: Text(
                    "49",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top:205,
              left:10,
              child: ButtonTheme(
                padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 10),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: listaStores[3].getColor(),
                  onPressed: () {},
                  child: Text(
                    "50",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top:255,
              left:10,
              child: ButtonTheme(
                padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 10),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: listaStores[4].getColor(),
                  onPressed: () {},
                  child: Text(
                    "51",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top:305,
              left:10,
              child: ButtonTheme(
                padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 10),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: listaStores[5].getColor(),
                  onPressed: () {},
                  child: Text(
                    "52",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            ),

            Positioned(
              top:105,
              left:205,
              child: ButtonTheme(
                padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 10),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: listaStores[6].getColor(),
                  onPressed: () {},
                  child: Text(
                    "160",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top:105,
              left:305,
              child: ButtonTheme(
                padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 10),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: listaStores[12].getColor(),
                  onPressed: () {},
                  child: Text(
                    "197",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top:155,
              left:205,
              child: ButtonTheme(
                padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 10),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: listaStores[7].getColor(),
                  onPressed: () {},
                  child: Text(
                    "161",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top:155,
              left:305,
              child: ButtonTheme(
                padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 10),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: Theme.of(context).accentColor,
                  onPressed: () {},
                  child: Text(
                    "196",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top:205,
              left:205,
              child: ButtonTheme(
                padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 10),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: listaStores[8].getColor(),
                  onPressed: () {},
                  child: Text(
                    "162",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top:205,
              left:305,
              child: ButtonTheme(
                padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 10),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: listaStores[11].getColor(),
                  onPressed: () {},
                  child: Text(
                    "195",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top:255,
              left:205,
              child: ButtonTheme(
                padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 10),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: listaStores[9].getColor(),
                  onPressed: () {},
                  child: Text(
                    "163",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top:255,
              left:305,
              child: ButtonTheme(
                padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 10),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: listaStores[10].getColor(),
                  onPressed: () {},
                  child: Text(
                    "194",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ),
              ),
            ),
          ],
            )

        );
  }
}

class StoreMap{

  var storeNumber;
  Color colorStore = Color(0xFF3fa535);

  storeMap(var numberS, Color color){
    this.storeNumber=numberS;
        this.colorStore=color;
  }

  setColor(Color color){
    this.colorStore = color;
  }
  setNumber(int nombre)
  {
    storeNumber=nombre;
  }
  getColor()
  {
    return this.colorStore;
  }





}