import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mercado_pulgas/blocs/category/categories.dart';
import 'package:mercado_pulgas/models/category.dart';
import 'package:mercado_pulgas/ui/widgets/loading_progress.dart';

class CategoriesCarousel extends StatelessWidget {

  Widget _getCategoryCard(BuildContext context, Category category) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: <Widget>[
        Container(
          height: 200.0,
          width: 150.0,
          margin: EdgeInsets.only(
              //left: 20.0
          ),

          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [Colors.green, Colors.blue]),
              image: DecorationImage(
                fit: BoxFit.cover,
                image: CachedNetworkImageProvider(category.imgUrl),
              ),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              shape: BoxShape.rectangle,
              boxShadow: <BoxShadow> [
                BoxShadow(
                    color: Colors.black38,
                    blurRadius: 15.0,
                    offset: Offset(0.0, 7.0)
                )
              ]
          ),
        ),
        Container(
          width: 150.0,
          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
          decoration: BoxDecoration(
            color: Colors.black38,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(10.0),
              bottomRight: Radius.circular(10.0),
            ),
          ),
          child: Center(
            child: Text(
              category.name,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 16.0,
                color: Colors.white,
              ),
              overflow: TextOverflow.ellipsis,
            )
          ),
        )
      ],
    );

  }
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CategoriesBloc, CategoriesState>(
        builder: (context, state) {
          if (state is CategoriesLoadSuccess) {
            final categoryDetail = state.categories[0];
            return Container(
              margin: EdgeInsets.symmetric(vertical: 15.0),
              child:_getCategoryCard(context, categoryDetail)
            );
          } else {
            return LoadingProgress();
          }

        }
    );
  }
}