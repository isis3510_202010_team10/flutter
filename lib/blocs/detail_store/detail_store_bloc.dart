import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mercado_pulgas/blocs/detail_store/detail_store_state.dart';
import 'package:mercado_pulgas/blocs/detail_store/detail_store_event.dart';
import 'package:mercado_pulgas/resources/store/firebase_store_repository.dart';
import 'package:mercado_pulgas/resources/store/stores_repository.dart';

class DetailStoreBloc extends Bloc<DetailStoreEvent, DetailStoresState> {
  StreamSubscription _storesSubscription;
  final FirebaseStoreRepository _storeRepository;


  DetailStoreBloc({StoresRepository storeRepository })
      :_storeRepository = storeRepository;


  DetailStoresState get initialState => StoreLoadInProgress();

  @override
  Stream<DetailStoresState> mapEventToState(DetailStoreEvent event) async* {
    if (event is LoadStoreById) {
      yield* _mapStoreByIdLoadedToState(event);
    } else if (event is StoreUpdated) {
      yield* _mapStoreUpdateToState(event);
    }
  }

  Stream<DetailStoresState> _mapStoreUpdateToState(StoreUpdated event) async* {
    yield StoreLoadSuccess(event.store);
  }

  Stream<DetailStoresState> _mapStoreByIdLoadedToState(LoadStoreById event) async* {
    _storesSubscription?.cancel();
    _storesSubscription = _storeRepository.storeById(event.storeId).listen(
          (store) => add(StoreUpdated(store)),
    );
  }

}
