import 'package:equatable/equatable.dart';
import 'package:mercado_pulgas/models/store.dart';

abstract class DetailStoresState extends Equatable {
  const DetailStoresState();

  @override
  List<Object> get props => [];
}


class StoreLoadInProgress extends DetailStoresState {}

class StoreLoadSuccess extends DetailStoresState {
  final Store store;

  const StoreLoadSuccess([this.store]);

  @override
  List<Object> get props => [store];

  @override
  String toString() => 'StoreLoadSuccess { stores: $store }';
}
