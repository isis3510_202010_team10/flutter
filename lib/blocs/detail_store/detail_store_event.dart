import 'package:equatable/equatable.dart';
import 'package:mercado_pulgas/models/store.dart';

abstract class DetailStoreEvent extends Equatable {
  const DetailStoreEvent();

  @override
  List<Object> get props => [];
}

class LoadStoreById extends DetailStoreEvent {
  final String storeId;

  const LoadStoreById(this.storeId);

  @override
  List<Object> get props => [storeId];

  @override
  String toString() => 'LoadStoreById { storeId: $storeId }';
}

class StoreUpdated extends DetailStoreEvent {
  final Store store;

  const StoreUpdated(this.store);

  @override
  List<Object> get props => [store];

  @override
  String toString() => 'StoreUpdated { store: $store }';
}
