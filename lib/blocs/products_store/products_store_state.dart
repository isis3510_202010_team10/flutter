import 'package:equatable/equatable.dart';
import 'package:mercado_pulgas/models/product.dart';

abstract class ProductsStoreState extends Equatable {
  const ProductsStoreState();

  @override
  List<Object> get props => [];
}


class ProducsStoreLoadInProgress extends ProductsStoreState {}

class ProducsStoreLoadSuccess extends ProductsStoreState {
  final List<Product> products;

  const ProducsStoreLoadSuccess([this.products]);

  @override
  List<Object> get props => [products];

  @override
  String toString() => 'ProducsStoreLoadSuccess{ products: $products }';
}
