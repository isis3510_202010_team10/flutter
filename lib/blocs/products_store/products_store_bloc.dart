import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mercado_pulgas/blocs/products_store/products_store_event.dart';
import 'package:mercado_pulgas/blocs/products_store/products_store_state.dart';
import 'package:mercado_pulgas/resources/product/firebase_product_repository.dart';

class ProductsStoreBloc extends Bloc<ProductsStoreEvent, ProductsStoreState> {
  StreamSubscription _productSubscription;
  final FirebaseProductRepository _productRepository;


  ProductsStoreBloc({FirebaseProductRepository productRepository })
      :_productRepository = productRepository;


  ProductsStoreState get initialState => ProducsStoreLoadInProgress();

  @override
  Stream<ProductsStoreState> mapEventToState(ProductsStoreEvent event) async* {
    if (event is LoadProductsByStore) {
      yield* _mapProdusctsByStoreLoadedToState(event);
    } else if (event is ProductsByStoreUpdated) {
      yield* _mapStoreUpdateToState(event);
    }
  }

  Stream<ProductsStoreState> _mapStoreUpdateToState(ProductsByStoreUpdated event) async* {
    yield ProducsStoreLoadSuccess(event.products);
  }

  Stream<ProductsStoreState> _mapProdusctsByStoreLoadedToState(LoadProductsByStore event) async* {
    _productSubscription?.cancel();
    _productSubscription = _productRepository.productsByStore(event.storeNumber)
        .listen(
          (produtc) => add(ProductsByStoreUpdated(produtc)),
    );
  }

}
