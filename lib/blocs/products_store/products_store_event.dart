import 'package:equatable/equatable.dart';
import 'package:mercado_pulgas/models/product.dart';

abstract class ProductsStoreEvent extends Equatable {
  const ProductsStoreEvent();

  @override
  List<Object> get props => [];
}

class LoadProductsByStore extends ProductsStoreEvent {
  final int storeNumber;

  const LoadProductsByStore(this.storeNumber);

  @override
  List<Object> get props => [storeNumber];

  @override
  String toString() => 'LoadProductsByStore{ storeNumber: $storeNumber }';
}

class ProductsByStoreUpdated extends ProductsStoreEvent {
  final List<Product> products;

  const ProductsByStoreUpdated(this.products);

  @override
  List<Object> get props => [products];

  @override
  String toString() => 'ProductsByStoreUpdated { products: $products }';
}
