import 'package:equatable/equatable.dart';
import 'package:mercado_pulgas/models/store.dart';

abstract class ConnectivityState extends Equatable {
  const ConnectivityState();

  @override
  List<Object> get props => [];
}

class ConnectivityOn extends ConnectivityState {}

class ConnectivityFailure extends ConnectivityState {}

class ConnectivityLoading extends ConnectivityState {}