import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mercado_pulgas/blocs/connectivity/connectivity.dart';

class ConnectivityBloc extends Bloc<ConnectivityEvent, ConnectivityState> {

  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;


  ConnectivityState get initialState => ConnectivityLoading();

  @override
  Stream<ConnectivityState> mapEventToState(ConnectivityEvent event) async* {
    if (event is LoadConnectivityStatus) {
      yield* _loadConnectivityStatus();
    } else if (event is ConnectivityStatusChange) {
      yield* _updateConnectionStatus(event);
    }
  }

  Stream<ConnectivityState> _loadConnectivityStatus() async* {
    _connectivitySubscription?.cancel();
    await initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(
            (result) => add(ConnectivityStatusChange(result)));

  }

  Future<void> initConnectivity() async {
    ConnectivityResult result;
    try {
      result = await _connectivity.checkConnectivity();
      add(ConnectivityStatusChange(result));
    } on PlatformException catch (e) {
      print(e.toString());
    }
  }

  Stream<ConnectivityState> _updateConnectionStatus(ConnectivityStatusChange event) async* {
    switch (event.result) {
      case ConnectivityResult.wifi:
        String wifiName, wifiBSSID, wifiIP;

        try {
          if (Platform.isIOS) {
            LocationAuthorizationStatus status =
            await _connectivity.getLocationServiceAuthorization();
            if (status == LocationAuthorizationStatus.notDetermined) {
              status =
              await _connectivity.requestLocationServiceAuthorization();
            }
            if (status == LocationAuthorizationStatus.authorizedAlways ||
                status == LocationAuthorizationStatus.authorizedWhenInUse) {
              wifiName = await _connectivity.getWifiName();
            } else {
              wifiName = await _connectivity.getWifiName();
            }
          } else {
            wifiName = await _connectivity.getWifiName();
          }
        } on PlatformException catch (e) {
          print(e.toString());
          yield ConnectivityFailure();
        }

        try {
          if (Platform.isIOS) {
            LocationAuthorizationStatus status =
            await _connectivity.getLocationServiceAuthorization();
            if (status == LocationAuthorizationStatus.notDetermined) {
              status =
              await _connectivity.requestLocationServiceAuthorization();
            }
            if (status == LocationAuthorizationStatus.authorizedAlways ||
                status == LocationAuthorizationStatus.authorizedWhenInUse) {
              wifiBSSID = await _connectivity.getWifiBSSID();
            } else {
              wifiBSSID = await _connectivity.getWifiBSSID();
            }
          } else {
            wifiBSSID = await _connectivity.getWifiBSSID();
          }
        } on PlatformException catch (e) {
          print(e.toString());
          yield ConnectivityFailure();
        }

        try {
          wifiIP = await _connectivity.getWifiIP();
        } on PlatformException catch (e) {
          print(e.toString());
          yield ConnectivityFailure();
        }

        yield ConnectivityOn();
        break;
      case ConnectivityResult.mobile:
        yield ConnectivityOn();
        break;
      case ConnectivityResult.none:
        yield ConnectivityFailure();
        break;
      default:
        yield ConnectivityFailure();
        break;
    }
  }



}