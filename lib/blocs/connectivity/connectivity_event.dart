import 'package:connectivity/connectivity.dart';
import 'package:equatable/equatable.dart';
import 'package:mercado_pulgas/models/store.dart';

abstract class ConnectivityEvent extends Equatable {
  const ConnectivityEvent();

  @override
  List<Object> get props => [];
}

class LoadConnectivityStatus extends ConnectivityEvent {}

class ConnectivityStatusChange extends ConnectivityEvent {
  final ConnectivityResult result;

  const ConnectivityStatusChange(this.result);

  @override
  List<Object> get props => [result];
}

