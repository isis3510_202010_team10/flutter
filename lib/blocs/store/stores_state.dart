import 'package:equatable/equatable.dart';
import 'package:mercado_pulgas/models/store.dart';

abstract class StoresState extends Equatable {
  const StoresState();

  @override
  List<Object> get props => [];
}

class StoresLoadInProgress extends StoresState {}

class StoresLoadSuccess extends StoresState {
  final List<Store> stores;
  final List<Store> favorites;

  const StoresLoadSuccess([this.stores = const [], this.favorites = const []]);

  @override
  List<Object> get props => [stores, favorites];

  @override
  String toString() => 'StoresLoadSuccess { stores: $stores, '
      +'favorites: $favorites }';
}


class StoresLoadFailure extends StoresState {}