import 'package:equatable/equatable.dart';
import 'package:mercado_pulgas/models/store.dart';

abstract class StoresEvent extends Equatable {
  const StoresEvent();

  @override
  List<Object> get props => [];
}

class LoadStores extends StoresEvent {}



class StoresLoadSuccessE extends StoresEvent {}

class StoreAdded extends StoresEvent {
  final Store store;

  const StoreAdded(this.store);

  @override
  List<Object> get props => [store];

  @override
  String toString() => 'StoreAdded { store: $store }';
}

class StoreUpdateFavorite extends StoresEvent {
  final Store store;

  const StoreUpdateFavorite(this.store);

  @override
  List<Object> get props => [store];

  @override
  String toString() => 'StoreUpdateFavorite { store: $store }';
}

class StoreUpdated extends StoresEvent {
  final Store store;

  const StoreUpdated(this.store);

  @override
  List<Object> get props => [store];

  @override
  String toString() => 'StoreUpdated { store: $store }';
}

class StoreDeleted extends StoresEvent {
  final Store store;

  const StoreDeleted(this.store);

  @override
  List<Object> get props => [store];

  @override
  String toString() => 'StoreDeleted { store: $store }';
}

class StoresUpdated extends StoresEvent {
  final List<Store> stores;
  final List<Store> favorites;

  const StoresUpdated(this.stores, this.favorites);

  @override
  List<Object> get props => [stores, favorites];
}


class ClearCompleted extends StoresEvent {}

class ToggleAll extends StoresEvent {}