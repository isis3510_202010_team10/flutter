import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mercado_pulgas/blocs/store/stores.dart';
import 'package:mercado_pulgas/models/store.dart';
import 'package:mercado_pulgas/models/store_entity.dart';
import 'package:mercado_pulgas/resources/store/firebase_store_repository.dart';
import 'package:mercado_pulgas/resources/store/store_database.dart';

class StoresBloc extends Bloc<StoresEvent, StoresState> {
  StreamSubscription _storesSubscription;
  final FirebaseStoreRepository _storeRepository;
  final StoreDatabase _storeDatabase = StoreDatabase();

  StoresBloc({FirebaseStoreRepository storeRepository})
      :_storeRepository = storeRepository;

  StoresState get initialState => StoresLoadInProgress();

  @override
  Stream<StoresState> mapEventToState(StoresEvent event) async* {
    if (event is LoadStores) {
      yield* _mapStoresLoadedToState();
    } else if (event is StoreAdded) {
      yield* _mapStoreAddedToState(event);
    } else if (event is StoresUpdated) {
      yield* _mapTodosUpdateToState(event);
    } else if (event is StoreUpdateFavorite) {
      yield* _mapStoreUpdateFavoriteState(event);
    }
  }

  Stream<StoresState> _mapStoresLoadedToState() async* {
    await _storeDatabase.initDB();
    final favoritesEntities = await _storeDatabase.favoriteStores();
    final favorites = favoritesEntities
        .map((store) => Store.fromEntity(store))
        .toList();

    _storesSubscription?.cancel();
    _storesSubscription = _storeRepository.stores().listen(
          (stores) => add(StoresUpdated(stores, favorites)),
    );
  }

  Stream<StoresState> _mapStoreAddedToState(StoreAdded event) async* {
    if (state is StoresLoadSuccess) {
      final List<Store> updatedStores = List.from((state as StoresLoadSuccess).stores)
        ..add(event.store);
      yield StoresLoadSuccess(updatedStores);
    }
  }

  Stream<StoresState> _mapStoreUpdateFavoriteState(StoreUpdateFavorite event) async* {
    print('Update favorite 🕶 ${event.store}');
    await _storeDatabase.insertFavoriteStore(event.store.toEntity());
    add(LoadStores());
  }

  Stream<StoresState> _mapTodosUpdateToState(StoresUpdated event) async* {
    yield StoresLoadSuccess(event.stores, event.favorites);
  }

}
