import 'package:equatable/equatable.dart';
import 'package:mercado_pulgas/models/category.dart';

abstract class CategoriesEvent extends Equatable {
  const CategoriesEvent();

  @override
  List<Object> get props => [];
}

class LoadCategoriesByStore extends CategoriesEvent {
  final List<String> categories;

  const LoadCategoriesByStore(this.categories);

  @override
  List<Object> get props => [categories];
}

class CategoriesLoadSuccessE extends CategoriesEvent {}

class CategoryUpdated extends CategoriesEvent {
  final Category category;

  const CategoryUpdated(this.category);

  @override
  List<Object> get props => [category];

  @override
  String toString() => 'CategoryUpdated { category: $category }';
}

class CategoriesUpdated extends CategoriesEvent {
  final List<Category> categories;

  const CategoriesUpdated(this.categories);

  @override
  List<Object> get props => [categories];
}
