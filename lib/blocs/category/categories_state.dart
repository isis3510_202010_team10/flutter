import 'package:equatable/equatable.dart';
import 'package:mercado_pulgas/models/category.dart';

abstract class CategoriesState extends Equatable {
  const CategoriesState();

  @override
  List<Object> get props => [];
}

class CategoriesLoadInProgress extends CategoriesState {}

class CategoriesLoadSuccess extends CategoriesState {
  final List<Category> categories;

  const CategoriesLoadSuccess([this.categories = const []]);

  @override
  List<Object> get props => [categories];

  @override
  String toString() => 'CategoriesLoadSuccess { categories: $categories }';
}

class CategoriesLoadFailure extends CategoriesState {}