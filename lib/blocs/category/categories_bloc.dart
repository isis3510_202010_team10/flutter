import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mercado_pulgas/blocs/category/categories_event.dart';
import 'package:mercado_pulgas/blocs/category/categories_state.dart';
import 'package:mercado_pulgas/resources/category/firebase_category_repository.dart';


class CategoriesBloc extends Bloc<CategoriesEvent, CategoriesState> {
  StreamSubscription _categoriesSubscription;
  final FirebaseCategoryRepository _categoryRepository;

  CategoriesBloc({FirebaseCategoryRepository categoryRepository})
      :_categoryRepository = categoryRepository;


  CategoriesState get initialState => CategoriesLoadInProgress();

  @override
  Stream<CategoriesState> mapEventToState(CategoriesEvent event) async* {
    if (event is LoadCategoriesByStore) {
      yield* _mapCategoriesLoadedToState(event);
    } else if (event is CategoriesUpdated) {
      yield* _mapCategoriesUpdateToState(event);
    }
  }

  Stream<CategoriesState> _mapCategoriesLoadedToState(LoadCategoriesByStore event) async* {
    _categoriesSubscription?.cancel();
    _categoriesSubscription = _categoryRepository
        .categoriesByStore(event.categories)
        .listen(
          (categories) => add(CategoriesUpdated(categories)),
    );
  }

  Stream<CategoriesState> _mapCategoriesUpdateToState(CategoriesUpdated event) async* {
    yield CategoriesLoadSuccess(event.categories);
  }

}
