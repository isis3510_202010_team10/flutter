import 'package:equatable/equatable.dart';
import 'package:mercado_pulgas/models/product.dart';

abstract class ProductsState extends Equatable {
  const ProductsState();

  @override
  List<Object> get props => [];
}

class ProductsLoadInProgress extends ProductsState {}

class ProductsLoadSuccess extends ProductsState {
  final List<Product> products;

  const ProductsLoadSuccess([this.products = const []]);

  @override
  List<Object> get props => [products];

  @override
  String toString() => 'ProductsLoadSuccess { products: $products }';
}

class ProductsLoadFailure extends ProductsState {}