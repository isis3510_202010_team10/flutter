import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mercado_pulgas/blocs/product/products.dart';
import 'package:mercado_pulgas/models/product.dart';
import 'package:mercado_pulgas/resources/product/firebase_product_repository.dart';


class ProductsBloc extends Bloc<ProductsEvent, ProductsState> {
  StreamSubscription _productsSubscription;
  final FirebaseProductRepository _productRepository;

  ProductsBloc({FirebaseProductRepository productRepository})
      :_productRepository = productRepository;


  ProductsState get initialState => ProductsLoadInProgress();

  @override
  Stream<ProductsState> mapEventToState(ProductsEvent event) async* {
    if (event is LoadProducts) {
      yield* _mapStoresLoadedToState();
    } else if (event is ProductAdded) {
      yield* _mapStoreAddedToState(event);
    }else if (event is ProductsUpdated) {
      yield* _mapTodosUpdateToState(event);
    }
  }

  Stream<ProductsState> _mapStoresLoadedToState() async* {
    _productsSubscription?.cancel();
    _productsSubscription = _productRepository.products().listen(
          (products) => add(ProductsUpdated(products)),
    );
  }

  Stream<ProductsState> _mapStoreAddedToState(ProductAdded event) async* {
    if (state is ProductsLoadSuccess) {
      final List<Product> updatedStores = List
        .from((state as ProductsLoadSuccess).products)
        ..add(event.product);
      yield ProductsLoadSuccess(updatedStores);
    }
  }

  Stream<ProductsState> _mapTodosUpdateToState(ProductsUpdated event) async* {
    yield ProductsLoadSuccess(event.products);
  }

}
