import 'package:equatable/equatable.dart';
import 'package:mercado_pulgas/models/product.dart';

abstract class ProductsEvent extends Equatable {
  const ProductsEvent();

  @override
  List<Object> get props => [];
}

class LoadProducts extends ProductsEvent {}

class ProductsLoadSuccessE extends ProductsEvent {}

class ProductAdded extends ProductsEvent {
  final Product product;

  const ProductAdded(this.product);

  @override
  List<Object> get props => [product];

  @override
  String toString() => 'Product Added { product: $product }';
}

class ProductUpdated extends ProductsEvent {
  final Product product;

  const ProductUpdated(this.product);

  @override
  List<Object> get props => [product];

  @override
  String toString() => 'Product Updated { product: $product }';
}

class ProductsUpdated extends ProductsEvent {
  final List<Product> products;

  const ProductsUpdated(this.products);

  @override
  List<Object> get props => [products];
}


class ClearCompleted extends ProductsEvent {}

class ToggleAll extends ProductsEvent {}