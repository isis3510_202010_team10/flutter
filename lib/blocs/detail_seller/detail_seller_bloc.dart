import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mercado_pulgas/blocs/detail_seller/detail_seller_event.dart';
import 'package:mercado_pulgas/blocs/detail_seller/detail_seller_state.dart';
import 'package:mercado_pulgas/resources/seller/firebase_seller_repository.dart';

class DetailSellerBloc extends Bloc<DetailSellerEvent, DetailSellerState> {
  StreamSubscription _sellerSubscription;
  final FirebaseSellerRepository _sellerRepository;


  DetailSellerBloc({FirebaseSellerRepository sellerRepository })
      :_sellerRepository = sellerRepository;


  DetailSellerState get initialState => SellerLoadInProgress();

  @override
  Stream<DetailSellerState> mapEventToState(DetailSellerEvent event) async* {
    if (event is LoadSellerByName) {
      yield* _mapStoreByIdLoadedToState(event);
    } else if (event is SellerUpdated) {
      yield* _mapStoreUpdateToState(event);
    }
  }

  Stream<DetailSellerState> _mapStoreUpdateToState(SellerUpdated event) async* {
    yield SellerLoadSuccess(event.seller);
  }

  Stream<DetailSellerState> _mapStoreByIdLoadedToState(LoadSellerByName event) async* {
    _sellerSubscription?.cancel();
    _sellerSubscription = _sellerRepository.sellerByName(event.sellerName)
        .listen(
          (store) => add(SellerUpdated(store)),
    );
  }

}
