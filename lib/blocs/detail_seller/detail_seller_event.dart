import 'package:equatable/equatable.dart';
import 'package:mercado_pulgas/models/seller.dart';

abstract class DetailSellerEvent extends Equatable {
  const DetailSellerEvent();

  @override
  List<Object> get props => [];
}

class LoadSellerByName extends DetailSellerEvent {
  final String sellerName;

  const LoadSellerByName(this.sellerName);

  @override
  List<Object> get props => [sellerName];

  @override
  String toString() => 'LoadSellerByName{ sellerName: $sellerName }';
}

class SellerUpdated extends DetailSellerEvent {
  final List<Seller> seller;

  const SellerUpdated(this.seller);

  @override
  List<Object> get props => [seller];

  @override
  String toString() => 'SellerUpdated { seller: $seller }';
}
