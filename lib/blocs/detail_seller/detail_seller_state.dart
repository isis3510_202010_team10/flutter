import 'package:equatable/equatable.dart';
import 'package:mercado_pulgas/models/seller.dart';

abstract class DetailSellerState extends Equatable {
  const DetailSellerState();

  @override
  List<Object> get props => [];
}


class SellerLoadInProgress extends DetailSellerState {}

class SellerLoadSuccess extends DetailSellerState {
  final List<Seller> seller;

  const SellerLoadSuccess([this.seller]);

  @override
  List<Object> get props => [seller];

  @override
  String toString() => 'SellerLoadSuccess { seller: $seller }';
}
