import 'package:equatable/equatable.dart';
import 'package:mercado_pulgas/models/category_entity.dart';

class Category extends Equatable{
  final String id;
  final String imgUrl;
  final String name;

  Category({
    this.id,
    this.imgUrl,
    this.name
  });

  Category copyWith({
    String id,
    String imgUrl,
    String name
  }) {
    return Category(
        id: id ?? this.id,
        imgUrl: imgUrl ?? this.imgUrl,
        name: name ?? this.name
    );
  }

  @override
  List<Object> get props => [id, imgUrl, name];

  @override
  String toString() {
    return 'Category{id: $id, name: $name}';
  }

  CategoryEntity toEntity() {
    return CategoryEntity(
        id: id,
        name: name,
        imgUrl: imgUrl
    );
  }

  static Category fromEntity(CategoryEntity entity) {
    return Category(
        id: entity.id ?? '0',
        name: entity.name ?? 'Default name',
        imgUrl: entity.imgUrl ?? 'Default imgUrl'
    );
  }

}