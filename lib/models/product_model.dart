class Product {
  final int id;
  final String name;
  final int price;
  Product(this.id, this.name, this.price);
}