import 'package:equatable/equatable.dart';
import 'package:mercado_pulgas/models/seller_entity.dart';

class Seller extends Equatable{
  final String id;
  final String birthDate;
  final String identification;
  final String name;

  Seller({
    this.id,
    this.birthDate,
    this.identification,
    this.name
  });

  Seller copyWith({
    String id,
    String birthDate,
    String identification,
    String name
  }) {
    return Seller(
        id: id ?? this.id,
        birthDate: birthDate ?? this.birthDate,
        identification: identification ?? this.identification,
        name: name ?? this.name
    );
  }

  @override
  List<Object> get props => [id, birthDate, identification, name];

  @override
  String toString() {
    return 'Seller{id: $id, name: $name, identification: $identification}';
  }

  SellerEntity toEntity() {
    return SellerEntity(
        id: id,
        birthDate: birthDate,
        identification: identification,
        name: name
    );
  }

  static Seller fromEntity(SellerEntity entity) {
    return Seller(
        id: entity.id ?? '0',
        birthDate: entity.birthDate ?? 'Default Date',
        identification: entity.identification ?? 'Default Identification',
        name: entity.name ?? 'Default Name'
    );
  }

}