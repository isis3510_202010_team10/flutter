import 'package:cloud_firestore/cloud_firestore.dart';

class StoreEntity {
  final String id;
  final String name;
  final String description;
  final String location;
  final String imgUrl;
  final int storeNumber;
  final List<String> categories;
  var favorite;

  StoreEntity({
    this.id,
    this.name,
    this.description,
    this.location,
    this.imgUrl,
    this.storeNumber,
    this.categories,
    this.favorite: 0
  });

  void addFavorite() {
    this.favorite = 1;
  }

  bool isFavorite() {
    return this.favorite == 1;
  }

  void removeFavorite() {
    this.favorite = 0;
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'description': description,
      'location': location,
      'imgUrl': imgUrl,
      'storeNumber': storeNumber,
      'categories': categories,
      'favorite': favorite,
    };
  }

  Map<String, dynamic> toMapDatabase() {
    return {
      'id': id,
      'name': name,
      'description': description,
      'location': location,
      'imgUrl': imgUrl,
      'storeNumber': storeNumber,
      'categories': categories?.join(""),
      'favorite': favorite,
    };
  }

  static StoreEntity fromSnapshot(DocumentSnapshot snap) {
    return StoreEntity(
      id: snap.documentID,
      name: snap.data['Owner_name'],
      description: snap.data['Categories'],
      location: 'Local 2',
      storeNumber: snap.data['Store_number'],
      categories: (snap.data['Categories'] as String).split(','),
      imgUrl: snap.data['Image']
    );
  }

  @override
  String toString() {
    return 'Store{id: $id, name: $name, storeNumber: $storeNumber, '
        +'desciption: $description, favorite: $favorite}';
  }
}

final StoreEntity storeOne = StoreEntity(
    id: '1',
    name: 'Puesto 1',
    description: 'Puesto de venta de camiseta 👔',
    imgUrl: 'https://cdn.pixabay.com/photo/2020/03/31/16/18/rabbit-4988412_1280.jpg'
);

final StoreEntity storeTwo = StoreEntity(
    id: '2',
    name: 'Puesto 2',
    description: 'Puesto de ventas de joyas 💎',
    imgUrl: 'https://cdn.pixabay.com/photo/2016/02/08/07/42/diamond-1186139_1280.jpg'
);

final StoreEntity storeThree = StoreEntity(
    id: '3',
    name: 'Puesto 3',
    description: 'Puesto de ventas de stickers 🎟',
    imgUrl: 'https://cdn.pixabay.com/photo/2017/07/05/08/46/tag-2473794_1280.png'
);

final StoreEntity storeFour = StoreEntity(
    id: '4',
    name: 'Puesto 4',
    description: 'Puesto de ventas de Juguetes 🎈',
    imgUrl: 'https://cdn.pixabay.com/photo/2016/11/28/10/48/child-1864718_1280.jpg'
);

final StoreEntity storeFive = StoreEntity(
    id: '5',
    name: 'Puesto 5',
    description: 'Puesto de ventas de comida 🍩',
    imgUrl: 'https://cdn.pixabay.com/photo/2016/03/23/15/00/ice-cream-cone-1274894_1280.jpg'
);

final StoreEntity storeSix = StoreEntity(
    id: '6',
    name: 'Puesto 6',
    description: 'Puesto de ventas de vapers 🚬',
    imgUrl: 'https://cdn.pixabay.com/photo/2018/08/03/22/04/gentleman-blowing-ecig-vapor-3582839_1280.jpg'
);

final List<StoreEntity> stores = [storeOne, storeTwo, storeThree, storeFour, storeFive, storeSix];
