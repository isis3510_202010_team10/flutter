import 'package:cloud_firestore/cloud_firestore.dart';

class SellerEntity {
  final String id;
  final String birthDate;
  final String identification;
  final String name;

  SellerEntity({
    this.id,
    this.birthDate,
    this.identification,
    this.name
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'birthDate': birthDate,
      'identification': identification,
      'name': name,
    };
  }

  static SellerEntity fromSnapshot(DocumentSnapshot snap) {
    return SellerEntity(
        id: snap.documentID,
        birthDate: snap.data['FechaNacimiento'].toString(),
        identification: snap.data['Identificacion'].toString(),
        name: snap.data['Nombre']
    );
  }

  @override
  String toString() {
    return 'SellerEntity{id: $id, name: $name, identificacion: $identification}';
  }
}