import 'package:equatable/equatable.dart';
import 'package:mercado_pulgas/models/store_entity.dart';

class Store extends Equatable {
  final String id;
  final String name;
  final String description;
  final String location;
  final String imgUrl;
  final int storeNumber;
  final List<String> categories;
  var favorite;

  Store({
        this.id,
        this.name,
        this.description,
        this.location,
        this.imgUrl,
        this.categories,
        this.storeNumber,
        this.favorite: 0
      });

  Store copyWith({
    String id,
    String name,
    String description,
    String location,
    String imgUrl,
    int storeNumber,
    List<String> categories,
    int favorite
  }) {
    return Store(
      id: id ?? this.id,
      name: name ?? this.name,
      description: description ?? this.description,
      location: location ?? this.location,
      imgUrl: imgUrl ?? this.imgUrl,
      favorite: favorite ?? this.favorite,
      storeNumber: storeNumber ?? this.storeNumber,
      categories: categories ?? this.categories,
    );
  }

  @override
  List<Object> get props => [
    id,
    name,
    description,
    location,
    imgUrl,
    storeNumber,
    categories,
    favorite
  ];

  @override
  String toString() {
    return 'Store{id: $id, name: $name, desciption: $description,'
        + 'fav: $favorite, storeNumber: $storeNumber}';
  }

  StoreEntity toEntity() {
    return StoreEntity(
        id: id,
        name: name,
        description: description,
        location: location,
        imgUrl: imgUrl,
        storeNumber: storeNumber,
        categories: categories,
        favorite: favorite
    );
  }

  static Store fromEntity(StoreEntity entity) {
    return Store(
        id: entity.id ?? '0',
        name: entity.name ?? 'Default name',
        description: entity.description ?? 'Default description',
        location: entity.location ?? 'Default location',
        imgUrl: entity.imgUrl ?? 'Default imgUrl',
        storeNumber: entity.storeNumber ?? 0,
        categories: entity.categories ?? [],
        favorite: entity.favorite ?? 0
    );
  }
}
