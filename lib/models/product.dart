import 'package:equatable/equatable.dart';
import 'package:mercado_pulgas/models/product_entity.dart';

class Product extends Equatable{
  final String id;
  final String imgUrl;
  final String name;
  final String description;
  final int price;
  final int storeNumber;

  Product({
    this.id,
    this.imgUrl,
    this.price,
    this.name,
    this.description,
    this.storeNumber
  });

  Product copyWith({
    String id,
    String imgUrl,
    String name,
    String description,
    int price,
    int storeNumber
  }) {
    return Product(
      id: id ?? this.id,
      imgUrl: imgUrl ?? this.imgUrl,
      name: name ?? this.name,
      description: description ?? this.description,
      price: price ?? this.price,
      storeNumber: storeNumber ?? this.storeNumber
    );
  }

  @override
  List<Object> get props => [id, imgUrl, name, description, price, storeNumber];

  @override
  String toString() {
    return 'Product{id: $id, name: $name,'
        +'storeNumber: $storeNumber, price: $price}';
  }

  ProductEntity toEntity() {
    return ProductEntity(
      id: id,
      storeNumber: storeNumber,
      name: name,
      description: description,
      price: price,
      imgUrl: imgUrl
    );
  }

  static Product fromEntity(ProductEntity entity) {
    return Product(
      id: entity.id ?? '0',
      storeNumber: entity.storeNumber ?? 0,
      name: entity.name ?? 'Default Name',
      description: entity.description ?? 'Default description',
      price: entity.price ?? 0,
      imgUrl: entity.imgUrl ?? 'Default imgUrl'
    );
  }

}
