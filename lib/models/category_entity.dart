import 'package:cloud_firestore/cloud_firestore.dart';

class CategoryEntity {
  final String id;
  final String imgUrl;
  final String name;

  CategoryEntity({
    this.id,
    this.imgUrl,
    this.name
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'imgUrl': imgUrl,
      'name': name
    };
  }

  static CategoryEntity fromSnapshot(DocumentSnapshot snap) {
    return CategoryEntity(
        id: snap.documentID,
        imgUrl: snap.data['Image'],
        name: snap.data['Nombre']
    );
  }

  @override
  String toString() {
    return 'CategoryEntity{id: $id, name: $name}';
  }
}