import 'package:cloud_firestore/cloud_firestore.dart';

class ProductEntity {
  final String id;
  final String imgUrl;
  final String name;
  final String description;
  final int price;
  final int storeNumber;

  ProductEntity({
    this.id,
    this.imgUrl,
    this.name,
    this.description,
    this.price,
    this.storeNumber
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'imgUrl': imgUrl,
      'name': name,
      'description': description,
      'price': price,
      'storeNumber': storeNumber,
    };
  }

  static ProductEntity fromSnapshot(DocumentSnapshot snap) {
    return ProductEntity(
      id: snap.documentID,
      imgUrl: snap.data['Image'],
      name: snap.data['Nombre'],
      description: snap.data['Descripción'],
      price: snap.data['Precio'],
      storeNumber: snap.data['Store_number']
    );
  }

  @override
  String toString() {
    return 'ProductEntity{id: $id, storeNumber: $storeNumber,'
        +' name: $name, price: $price}';
  }
}