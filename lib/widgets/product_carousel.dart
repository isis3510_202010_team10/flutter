import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:mercado_pulgas/widgets/product_item.dart';

class ProductCarousel extends StatelessWidget {
  final String title;

  ProductCarousel(this.title);

  @override
  Widget build(BuildContext context) {
    final titleText = Text(
      title,
      style: TextStyle(
        fontSize: 24.0,
        fontWeight: FontWeight.bold
      ),
    );

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(
            horizontal: 15.0,
            vertical: 10.0
          ),
          child: titleText,
        ),
        Container(
          height: 250.0,
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 15.0),
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              ProductItem(),
              ProductItem(),
              ProductItem(),
              ProductItem(),
            ],
          ),
        )
      ],
    );
  }
}