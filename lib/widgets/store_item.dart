import 'package:flutter/material.dart';

class StoreItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final description = 'Ubicacion: Local 142 \nCategoria: Ropa y accesorios \nDescripcion: pequeña tienda';

    final storeDescription = Container(
      margin: EdgeInsets.only(
          left: 20.0
      ),
      child: Text(
        description,
        textAlign: TextAlign.left,
        style: TextStyle(
            fontSize: 13.0,
            fontWeight: FontWeight.normal
        ),
      ),
    );

    final storeName = Container(
      margin: EdgeInsets.only(
          left: 20.0
      ),
      child: Text(
        'Puesto 140',
        textAlign: TextAlign.left,
        style: TextStyle(
          fontSize: 17.0,
          fontWeight: FontWeight.w600
        ),
      ),
    );

    final photo = Container(
      margin: EdgeInsets.only(
          top: 20.0,
          left: 20.0
      ),
      width: 80.0,
      height: 80.0,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              fit: BoxFit.cover,
              image: NetworkImage('https://images.pexels.com/photos/3645369/pexels-photo-3645369.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260')
          )
      ),
    );

    final storeDetail = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        storeName,
        storeDescription
      ],
    );


    return Row(
      children: <Widget>[
        photo,
        storeDetail
      ],
    );
  }
}