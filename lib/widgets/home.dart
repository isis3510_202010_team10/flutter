import 'package:flutter/material.dart';

import 'banner_img.dart';
import 'product_carousel.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        BannerImg(),
        ProductCarousel('Recomendaciones'),
        ProductCarousel('Populares')
      ],
    );;
  }
}