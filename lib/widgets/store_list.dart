import 'package:flutter/material.dart';
import 'store_item.dart';

class StoreList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
      width: double.infinity,
      child: ListView(
        children: <Widget>[
          Text(
            'Tiendas',
            style: TextStyle(
              fontFamily: 'Muli',
              fontSize: 28.0,
              fontWeight: FontWeight.bold,
              color: Colors.black87
            ),
          ),
          StoreItem(),
          StoreItem(),
          StoreItem(),
          StoreItem(),
          StoreItem()
        ],
      ),
    );
  }
}