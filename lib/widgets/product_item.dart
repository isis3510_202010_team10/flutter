import 'package:flutter/material.dart';
import 'package:mercado_pulgas/screens/store_screen.dart';

class ProductItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final pathImg = 'assets/images/product_mock.jpg';

    final productImage = Container(
      height: 100.0,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage(pathImg)
        )
      ),
      child: Row(),
    );

    final productName = Container(
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      margin: EdgeInsets.symmetric(vertical: 5.0),
      child: Text(
        'Un producto aleatorio',
        style: TextStyle(
            fontSize: 18.0,
            fontWeight: FontWeight.w600
        ),
        textAlign: TextAlign.center,
      ),
    );

    final productPrice = Container(
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      margin: EdgeInsets.symmetric(vertical: 0.0),
      child: Text(
        '\$14.000',
        style: TextStyle(
          color: Theme.of(context).accentColor,
            fontSize: 16.0,
            fontWeight: FontWeight.bold
        ),
        textAlign: TextAlign.center,
      ),
    );

    final productButton = FlatButton(
      padding: EdgeInsets.all(4.0),
      onPressed: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (_) => StoreScreen()
        )
      ),
      color: Theme.of(context).buttonColor,
      textColor: Colors.white,
      child: Text(
        'Ir',
        style: TextStyle(
          fontSize: 16.0,
          fontWeight: FontWeight.w600
        ),
      ),
    );

    return Container(
      margin: EdgeInsets.all(10.0),
      width: 150.0,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            offset: Offset(0.0, 2.0),
            blurRadius: 6.0,
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          productImage,
          productName,
          productPrice,
          productButton
        ],
      ),
    );
  }
}