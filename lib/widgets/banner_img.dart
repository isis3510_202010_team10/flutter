import 'package:flutter/material.dart';

class BannerImg extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final pathImg = 'assets/images/logo_mercado.png';

    return Container(
      margin: EdgeInsets.only(
        bottom: 20.0
      ),
      height: 250.0,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        image: DecorationImage(
          image: AssetImage(pathImg)
        )
      ),
    );
  }
}